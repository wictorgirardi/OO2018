#ifndef CANETA_HPP
#define CANETA_HPP
#include <string>

class Caneta {
	//atributos
private:
    std::string cor;
    std::string marca;
float preco;

	//metodos
public:
Caneta();   //Construtor
~Caneta(); //Destrutor
//metodos acessores Get/ Set

    void setCor(std::string cor);
    std::string getCor();
    void setMarca(std::string Marca);
    std::string getMarca();
void setPreco(float preco);
float getPreco();
void imprimeDados();
//outro metodo 
};

#endif
