#include "caneta.hpp"
#include <iostream>
Caneta::Caneta(){
    std::cout << "Construtor da caneta"<< std::endl;
    cor = "Branco";
    marca = "Genérica";
    preco = 1.0;
}
Caneta::~Caneta(){
std::cout << "Destrutor da caneta" << std::endl;
}
void Caneta::setCor(std::string cor){
this->cor = cor;
}
std::string Caneta:: getCor(){
    return cor;
}

void Caneta::setMarca(std::string marca){
    this->marca = marca;
}

void Caneta::setPreco(float preco){
    this->preco = preco;
}

float Caneta::getPreco(){
    return preco;
}
void Caneta::imprimeDados(){
    std::cout << "Cor: " << cor << std::endl;
    std::cout << "Marca: "<< marca << std::endl;
    std::cout << "Preco: "<< preco << std::endl;
}
